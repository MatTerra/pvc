import cv2 as cv
import numpy as np
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=False, help="Path to the image")
ap.add_argument("-v", "--video", required=False, help="Path to video")
args = vars(ap.parse_args())


def id_color(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        global color
        color = img[y, x]
        if not bw:
            print "Coordenadas: ", (x, y), "   Cor:  ", color
        else:
            print "Coordenadas: ", (x, y), "   Intensidade do Pixel:  ", color

bw=False
color = None
video = False
real_time = False
if args.get("image"):
    img = cv.imread(args["image"])
    imgBW = cv.imread(args["image"], cv.IMREAD_GRAYSCALE)
    difference = cv.subtract(img, cv.cvtColor(imgBW, cv.COLOR_GRAY2BGR))
    b, g, r = cv.split(difference)
    if cv.countNonZero(b) == 0 and cv.countNonZero(g) == 0 and cv.countNonZero(r) == 0:
        bw = True
        img = imgBW
elif not args.get("video") == 'r':
    cam = cv.VideoCapture(args.get("video"))
    frame_counter = 0
    video = True
else:
    cam = cv.VideoCapture(-1)
    video = True
    real_time = True
cv.namedWindow("Identificador de Cor")
cv.setMouseCallback("Identificador de Cor", id_color)
while not (cv.waitKey(25) & 0xFF == ord('q') or cv.getWindowProperty('Identificador de Cor', 0)):
    if video:
        if not cam.isOpened():
            cam.open(0)
            continue
        ret, img = cam.read()
        if not real_time:
            frame_counter += 1
            # Reinicia o video
            if frame_counter == cam.get(cv.CAP_PROP_FRAME_COUNT):
                frame_counter = 0
                cam.set(cv.CAP_PROP_POS_FRAMES, frame_counter)
    img2 = img.copy()
    if color is not None:
        if not bw:
            # Separa as 3 cores
            R = img2[:, :, 2].astype(np.float32)
            G = img2[:, :, 1].astype(np.float32)
            B = img2[:, :, 0].astype(np.float32)
            sq_dist = ((B - color[0]) ** 2) + ((G - color[1]) ** 2) + ((R - color[2]) ** 2)
            mask = sq_dist < (13 ** 2)
            # Cria um vetor com True nos pixels que estao mais proximos que 13 (Euclidiano)
        else:
            inten = img2[:, :].astype(np.float32)
            dist = (inten-color)**2
            mask = dist < 13**2
            img2 = cv.cvtColor(img2, cv.COLOR_GRAY2BGR)
        img2[mask] = (0, 0, 255)
    cv.imshow("Identificador de Cor", img2)
cv.destroyAllWindows()

