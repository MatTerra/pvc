import cv2 as cv


def id_color(event, x, y, flags, param):
    if event == cv.EVENT_LBUTTONDOWN:
        global color
        color = img[y, x]
        img2 = cv.inRange(img, (color[0]-13, color[1]-13, color[2]-13), (color[0]+13, color[1]+13,color[2]+13))
        cv.imshow("Mascara",img2)


img = cv.imread("../data/colors.png")
cv.namedWindow("Identificador de Cor")
cv.setMouseCallback("Identificador de Cor", id_color)
cv.imshow("Identificador de Cor", img)
while(True):
    cv.waitKey(1)

