\documentclass{bmvc2k}

%% Enter your paper number here for the review copy
% \bmvcreviewcopy{??}

% \usepackage[brazilian]{babel}
\usepackage[utf8]{inputenc}

\title{Reconhecimento de cores em vídeos e imagens utilizando OpenCV e Python}

% Enter the paper's authors in order
% \addauthor{Name}{email/homepage}{INSTITUTION_CODE}
\addauthor{Mateus Berardo S. Terra}{mateus.berardo@brino.cc}{1}


% Enter the institutions
% \addinstitution{Name\\Address}
\addinstitution{
  Departamento de Ci\^encia da Comptuta\c{c}\~ao\\
  Universidade de Bras\'{\i}lia\\
  Campus Darcy Ribeiro, Asa Norte\\
  Bras\'{\i}lia-DF, CEP 70910-900, Brazil,  
}

\runninghead{Mateus Berardo}{Detecção de cores em vídeos e imagens -- \today}

% Any macro definitions you would like to include
% These are not defined in the style file, because they don't begin
% with \bmva, so they might conflict with the user's own macros.
% The \bmvaOneDot macro adds a full stop unless there is one in the
% text already.
\def\eg{\emph{e.g}\bmvaOneDot}
\def\Eg{\emph{E.g}\bmvaOneDot}
\def\etal{\emph{et al}\bmvaOneDot}

%-------------------------------------------------------------------------
% Document starts here
\begin{document}

\maketitle

\begin{abstract}
Esse trabalho busca desenvolver um sistema capaz de identificar cores, mediante seleção de pixel pelo usuário, e destacar seções que possuem cores próximas, tanto em imagens quanto em vídeos. Para definir cores próximas, foi utilizado o conceito de distância euclidiana, para imagens em RGB, e a intensidade do pixel para imagens em tons de cinza. Durante o desenvolvimento, a otimização do código para realizar operações em matrizes foi essencial, principalmente para o processamento de vídeos. Outro objetivo desse trabalho é a exploração da biblioteca OpenCV que é discutida neste documento.

\end{abstract}

%-------------------------------------------------------------------------
\section{Introdução}
\label{sec:intro}
O reconhecimento de cores é um dos desafios mais básicos da visão computacional e é o ponto de partida para análises mais complexas. A partir da análise da cor, é possível a identificação de bordas, formas, entre outras características da imagem.

Neste trabalho desenvolvemos um sistema de identificação de cor para imagens e videos. Este deve reagir à seleção de um pixel pelo usuário com o mouse e informar pelo terminal a posição e cor da seleção além de marcar em vermelho outros pixels com cores próximas.

Para este trabalho definiremos cores próximas das seguintes formas: Em imagens em tons de cinza, em que apenas um canal define a cor, ou a intensidade do pixel ($i_{0}$), serão próximos os pixels cuxa intensidade $i_{1}$ satisfaça a inequação \ref{eq:proximo}:

\begin{equation}
\label{eq:proximo}	|i_{1}-i_{0}| < 13
\end{equation}

Para imagens ou vídeos em RGB utilizaremos a distância euclidiana, definida como sendo a raiz quadrada da soma dos quadrados das distâncias em cada dimensão, como demonstrado na equação \ref{eq:euclid}. Definiremos $R_{0}$, $G_{0}$ e $B_{0}$ como os valores dos canais selecionados pelo usuário e $R_{1}$, $G_{1}$ e $B_{1}$ como os valores dos canais do pixel que estamos comparando e que será considerado próximo caso satisfaça a inequação \ref{eq:euclidProximo}.

Para processar as imagens utilizaremos a biblioteca OpenCV\cite{[OpenCV]}. 

\begin{equation}
\label{eq:euclid}	
d_{ec}=\sqrt{ |R_{1}-R_{0}|^2+|G_{1}-G_{0}|^2+|B_{1}-B_{0}|^2}
\end{equation}

\begin{equation}
\label{eq:euclidProximo}	d_{ec} < 13
\end{equation}

\subsection{Outros objetivos}

Além do desenvolvimento da solução, objetivamos adquirir conhecimentos técnicos para configurar e utilizar a biblioteca OpenCV. Obtendo ao final um ambiente de trabalho funcional e familiaridade com o módulo e com a API da biblioteca.

\subsection{Repositório}

O código completo desenvolvido para este documento está disponível em um repositório git hospedado no GitLab no endereço \url{https://gitlab.com/MatTerra/pvc}.


\section{Metodologia}
\label{sec:metod}
\subsection{Sistema}
O desenvolvimento foi realizado em uma máquina virtual Ubuntu 16.04 x64 com python 2.7 e a biblioteca OpenCV\cite{[OpenCV]} 3.2.0. Foram alocados 4Gb de RAM e 2 núcleos para a máquina virtual. Foi utilizada a IDE PyCharm para desenvolvimento e depuração.

Para a instalação, foram utilizados os passos descritos por Adrian Rosebrock\cite{[instalacao]} com adaptações para as versões descritas acima.

\subsection{Requisitos}
O desenvolvimento do sistema deveria atender quatro requisitos, descritos a seguir:
\begin{description}
	\item[Primeiro requisito:] O programa deve identificar, quando o usuário clicar em um pixel, a cor e a posição deste e informá-lo no terminal. Caso a imagem esteja em escala de cinza, deve ser informada a intensidade do pixel.
	\item[Segundo requisito:] Além de informar a cor, o programa deve destacar na imagem, em vermelho, pixels que tem cores ou intensidade próximas ao selecionado pelo usuário. Para imagens coloridas a distância será calculada pela distância euclidiana descrita na introdução, e em escala de cinza, por meio da diferença de intensidade. Para ambos, a tolerância será de 13 unidades.
	\item[Terceito requisito:] O programa deve realizar as mesmas tarefas dos dois primeiros requisitos em um vídeo disponível no disco do computador.
	\item[Quarto requisito:] O programa deverá executar o terceiro requisito em um vídeo em tempo real capturado por uma webcam integrada ou conectada pela porta USB.
\end{description}

\subsection{Testes com Imagens}
Inicialmente foi desenvolvida a detecção da cor do pixel selecionado pelo usuário com o mouse. Para isso, foi implementado um {\em handler} que captura o clique e sua posição e acessa a imagem para registrar a cor em uma variável. Além disso, essa função registra no terminal a posição do clique e a cor detectada ou a intensidade do pixel para imagens em preto e branco, como demonstrado na figura \ref{fig:terminal}.

\begin{figure}[h!]
	\begin{tabular}{ccc}
		\bmvaHangBox{\includegraphics[height=3.5cm]{Figs/colorsOutput.png}}&
		\bmvaHangBox{\includegraphics[height=3.5cm]{Figs/pb.png}}\\\\
		(a)&(b)
	\end{tabular}
	\caption{(a) Saída do terminal para uma imagem colorida de um disco de cores mediante clique na área vermelha; (b) Saída do terminal para imagem em tons de cinza.}
	\label{fig:terminal}
\end{figure}

Durante o desenvolvimento do sistema de destaque, proposto incialmente no segundo requisito, foram realizados vários testes com diferentes abordagens. Inicialmente, foi testada a função {\em inRange} \cite{[inRange]} do módulo {\em OpenCV}, que identifica pixels com cores em um range definido. Na figura \ref{fig:mascara} podemos ver uma máscara resultante dessa função quando selecionamos um pixel próximo à borda verde na imagem do disco de cores. Como esta função utiliza distâncias fixas para cada canal de cor na imagem, obtemos a partir dela uma máscara que é aproximadamente um quadrilátero.

\begin{figure}[h!]
	\begin{tabular}{ccc}
		\bmvaHangBox{\fbox{\includegraphics[width=5.6cm]{Figs/colors.png}}}&
		\bmvaHangBox{\fbox{\includegraphics[width=5.6cm]{Figs/mask.png}}}\\
		(a)&(b)
	\end{tabular}
	\caption{Máscara (b) resultante da função inRange em um disco de cores (a).}
	\label{fig:mascara}
\end{figure}



De acordo com o requisito proposto, em uma imagem colorida devem ser destacados os pixels cujas cores estejam em uma distância euclidiana menor que 13, conforme a equação 1, portanto, a função {\em inRange} \cite{[inRange]} apenas aproxima a área que deve ser destacada, não sendo o suficiente sozinha para o cumprimento dos requisitos.

Para solucionar esse problema, depois de criada a máscara, a imagem era percorrida por um conjunto de laços{\em for} em que os pixels destacados na máscara tinham sua cor verificada de acordo com a distância euclidiana à cor selecionada e, então, estes eram marcados ou não. Contudo, esta abordagem se mostrou lenta demais para o processamento em tempo real e foi abandonada.

Em seguida, foi testada uma abordagem matricial, como descrito por dobkind\cite{[stack]} Inicialmente, os canais de cores são separados para em seguida, a distância ao quadrado ser calculada. Com estes valores, geramos uma máscara booleana para marcar os itens da matriz que satisfazem a condição do requisito proposto. Em seguida, utilizamos a inserção booleana de {\em ndarrays} para substituir a cor dos pixels marcados por vermelho. 

\texttt{R = img2[:, :, 2].astype(np.float32)}

\texttt{G = img2[:, :, 1].astype(np.float32)}

\texttt{B = img2[:, :, 0].astype(np.float32)}

\texttt{sq\_dist=((B-color[0])**2)+((G-color[1])**2)+((R-color[2])**2)}

\texttt{mask = sq\_dist < (13 ** 2)}

\texttt{img2[mask] = (0, 0, 255)}


\section{Resultados}

A abordagem matricial, além de precisa, é rápida o suficiente para ser utilizada com vídeos em tempo real, portanto foi a escolhida para o projeto final, que atendeu a todos os requisitos propostos. Na figura \ref{fig:imgFinal} podemos ver o resultado em uma imagem.\\

\begin{figure}[h!]
	\begin{tabular}{ccc}
		\bmvaHangBox{\fbox{\includegraphics[width=5.6cm]{Figs/colors.png}}}&
		\bmvaHangBox{\fbox{\includegraphics[width=5.6cm]{Figs/destaque.png}}}\\
		(a)&(b)
	\end{tabular}
	\caption{Imagem com pixels com cor próxima à selecionada em vermelho (b) em comparação ao disco original (a).}
	\label{fig:imgFinal}
\end{figure}
	
Além das imagens, processamos vídeos buscando identificar cores próximas. Com a otimização do código, possível em especial pelo uso de operações matriciais, a adaptação do código para vídeos consistiu em alternar a fonte de imagens para os frames do arquivo ou do dispositivo de captura. Na figura \ref{fig:videos} estão expostos os resultados em dois vídeos, um ao vivo e outro previamente gravado.

\begin{figure}[h!]
	\begin{tabular}{ccc}
		\bmvaHangBox{\fbox{\includegraphics[height=4cm]{Figs/video.png}}}&
		\bmvaHangBox{\fbox{\includegraphics[height=4cm]{Figs/videoReal.png}}}\\\\
		(a)&(b)
	\end{tabular}
	\caption{Frame de vídeo em tempo real com destaque vermelho em pixels com cores parecidas (b); Frame de animação obtida em banco de imagens com destaque em pixels parecidos (a).}
	\label{fig:videos}
\end{figure}

O programa utiliza argumentos de linha de comando para definir a fonte de mídia que processará, as opções disponíveis são:

\begin{description}
	
	\item[-i <caminho>] abre uma imagem em {\em<caminho>};
	\item[-v <caminho>] abre um vídeo em {\em<caminho>};
	\item[-vr] abre a câmera para captura de vídeo em tempo real.
\end{description}

\section{Conclusão}
O trabalho foi bem sucedido e concluímos que a seleção de cor por distância euclidiana ou pela diferença de intensidade dos pixels é possível e facilmente implementada. Além disso, cumprimos os objetivos secundários descritos na introdução. É evidente também a necessidade de buscar meios eficientes para a manipulação de matrizes, uma vez que imagens cada vez maiores deverão sofrer cada vez mais operações e devem ser exibidas em tempo real. 


\bibliography{refs}
\end{document}
